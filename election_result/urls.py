"""election_result URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from rest_framework.authtoken.views import ObtainAuthToken
from winner.views import UserProfileViewSet, ResultVoteViewSet
from winner.views import ElectionDataViewSet, AllyGainLoseXmlViewSet, CandidateVipFlashesViewset
from winner.views import CandidateViewset, AllyVoteShareXmlViewSet, AllyGainLoseRegionXmlViewSet
router = routers.DefaultRouter()
router.register(r'api/v1/vip_flashes', CandidateVipFlashesViewset, base_name='vip_flashes')
router.register(r'api/v1/gail_lose_region', AllyGainLoseRegionXmlViewSet, base_name='gail_lose_region')
router.register(r'api/v1/vote_share', AllyVoteShareXmlViewSet, base_name='vote_share')
router.register(r'api/v1/candidate_data', CandidateViewset, base_name='candidate_data')
router.register(r'api/v1/ally_gain_lose', AllyGainLoseXmlViewSet, base_name='ally_gain_lose')
router.register(r'api/v1/election_data', ElectionDataViewSet, base_name='election_data')
router.register(r'api/v1/resultvote', ResultVoteViewSet, base_name='resultvote')
router.register(r'api/v1/profile', UserProfileViewSet, base_name='profile')
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/auth_token', ObtainAuthToken.as_view()),
    url(r'^', include(router.urls)),
]
urlpatterns += staticfiles_urlpatterns()