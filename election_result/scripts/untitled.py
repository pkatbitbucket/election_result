def read_karnataka_result_compare_2014(year):
    win_status_dict = {
        1: 'L',
        2: 'RunnerUp',
        3: 'SecondRunnerUp',
    }
    election_2013, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2013'
    )
    election_2014, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2014'
    )
    if year==2013:
        election = election_2013
    if year==2014:
        election = election_2014
    state_obj, created = State.objects.get_or_create(name='Karnataka')
    file_name = 'Karnataka_13_14_results_compare_{0}.csv'.format(year)
    with open(file_name, "rb") as f:
        reader = csv.reader(f)
        counter = 0
        for row in reader:
            if counter>0:
                Ac_No,AC_name,region,WIN,RUP,SRUP,WIN_VOTES,RUP_VOTES, \
                SRUP_VOTES,MOV,PMOV,Total,AAAP,AIFB,AITC,ANC,asdp, \
                BDBRAJP,BHPP,BJP,BMUP,BSP,CPI,CPI_ML_L,CPIM,CPM,DPPS, \
                GaAP,HJP,INC,IND,JD_S,JD_U,JVBP,KAP,KCVP,KDC,LSP,NCP, \
                NDEP,PPOI,RCMP,RKSP,RPI_A,RSPS,SDPI,SJPA,SP,SUCI,VJCP,
                WPOI,Grand_Total = row
                region, created = Region.objects.get_or_create(name=region)
                constituency_obj, created = Constituency.objects.get_or_create(
                    state_id=state_obj.id, name=AC_name, 
                )
                constituency_obj.region = region
                constituency_obj.save()
                parties_name = [
                    "ABHM","ABML(S)","ADMK","AIFB(S)","AKBRP","AMJP","ANC",
                    "BDBRAJP","BhJD","BHPP","BJP","BPJP","BRPP","BSP","BSRCP",
                    "CPI","CPI(ML)(L)","CPIM","CPM","DPPS","HJP","HND","INC",
                    "IND","INL","IUML","JD(S)","JD(U)","JVBP","KAP","KaSP",
                    "KCVP","KDC","KJP","KMP","KRRS","LJP","LSP","MPHP","NCP",
                    "NDEP","NPP","PPIS","PPOI","RCMP","RPI","RPI(A)","RSPS",
                    "SAJP","SDP","SDPI","SHS","SJKP","SJPA","SK","SKP","SP",
                    "SUCI","VJCP","WPOI"
                ]
                parties_name_dict = {
                    "ABHM": ABHM, "ABML(S)": ABML_S, "ADMK": ADMK, "AIFB(S)": AIFB_S,
                    "AKBRP": AKBRP, "AMJP": AMJP, "ANC": ANC, "BDBRAJP": BDBRAJP, "BhJD": BhJD,
                    "BHPP": BHPP, "BJP": BJP, "BPJP": BPJP, "BRPP": BRPP, "BSP": BSP, "BSRCP": BSRCP,
                    "CPI": CPI, "CPI(ML)(L)": CPI_ML_L, "CPIM": CPIM, "CPM": CPM,
                    "DPPS": DPPS, "HJP": HJP, "HND": HND, "INC": INC, "IND": IND,
                    "INL": INL, "IUML": IUML, "JD(S)": JD_S, "JD(U)": JD_U,
                    "JVBP": JVBP, "KAP": KAP, "KaSP": KaSP, "KCVP": KCVP, "KDC": KDC,
                    "KJP": KJP, "KMP": KMP, "KRRS": KRRS, "LJP": LJP, "LSP": LSP,
                    "MPHP": MPHP, "NCP": NCP, "NDEP": NDEP, "NPP": NPP, "PPIS": PPIS,
                    "PPOI": PPOI, "RCMP": RCMP, "RPI": RPI, "RPI(A)": RPI_A, "RSPS": RSPS,
                    "SAJP": SAJP, "SDP": SDP, "SDPI": SDPI, "SHS": SHS, "SJKP": SJKP,
                    "SJPA": SJPA, "SK": SK, "SKP": SKP, "SP": SP, "SUCI": SUCI,
                    "VJCP": VJCP, "WPOI": WPOI,
                }
                Party_vote = collections.namedtuple('Party_vote', 'votes party_name')
                best_votes = sorted([Party_vote(v,k) for (k,v) in parties_name_dict.items()], reverse=True)
                for level, party_vote in enumerate(best_votes):
                    party_name = party_vote.party_name
                    votes = party_vote.votes
                    party_obj, created = Party.objects.get_or_create(name=party_name, abbr=party_name)
                    group_obj, created = Group.objects.get_or_create(name=party_name)
                    group_obj.parties.add(party_obj)
                    group_obj.save()
                    election_candidate_objs = ElectionCandidate.objects.filter(
                        election_id=election_2013.id,
                        constituency_id=constituency_obj.id,
                        party_id=party_obj.id
                    )
                    if year==2013:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj.group=group_obj
                            election_candidate_obj.save()
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                                party_obj.name, constituency_obj.name
                            )
                            continue
                    elif year==2014:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj, created = ElectionCandidate.objects.get_or_create(
                                election_id=election_2014.id,
                                constituency_id=constituency_obj.id,
                                party_id=party_obj.id,
                                candidate_id=election_candidate_obj.candidate.id,
                                group_id=group_obj.id
                            )
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                                party_obj.name, constituency_obj.name
                            )
                            continue
                    result_vote, created = ResultVote.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        result_vote.level=level + 1
                        win_status = win_status_dict.get(level, 'Lost')
                        result_vote.votes=votes
                        result_vote.status=win_status
                        result_vote.is_result_final=True
                        result_vote.save()
                    result_vote_history, created = ResultVoteHistory.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        win_status = win_status_dict.get(level, 'Lost')
                        result_vote_history.level = level + 1
                        result_vote_history.votes = votes
                        result_vote_history.is_result_final = True
                        result_vote_history.status=win_status
                        result_vote_history.save()
                    election_result_obj, created = ElectionResult.objects.get_or_create(
                        election=election,
                        election_candidate=election_candidate_obj,
                        candidate=candidate_obj,
                        state=state_obj,
                        constituency=constituency_obj,
                        party=party_obj
                    )
                    if votes:
                        election_result_obj.vote=votes
                        election_result_obj.group=party_obj.group_set.all()[0]
                        election_result_obj.save()
            counter += 1