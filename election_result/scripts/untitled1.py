def read_karnataka_result_compare_2014(year):
    win_status_dict = {
        1: 'L',
        2: 'RunnerUp',
        3: 'SecondRunnerUp',
    }
    election_2013, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2013'
    )
    election_2014, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2014'
    )
    if year==2013:
        election = election_2013
    if year==2014:
        election = election_2014
    state_obj, created = State.objects.get_or_create(name='Karnataka')
    file_name = 'Karnataka_13_14_results_compare_{0}.csv'.format(year)
    with open(file_name, "rb") as f:
        reader = csv.reader(f)
        counter = 0
        bjp_counter = 0
        for row in reader:
            if counter>0:
                Ac_No,AC_name,region,WIN,RUP,SRUP,WIN_VOTES,RUP_VOTES, \
                SRUP_VOTES,MOV,PMOV,Total,AAAP,AIFB,AITC,ANC,asdp, \
                BDBRAJP,BHPP,BJP,BMUP,BSP,CPI,CPI_ML_L,CPIM,CPM,DPPS, \
                GaAP,HJP,INC,IND,JD_S,JD_U,JVBP,KAP,KCVP,KDC,LSP,NCP, \
                NDEP,PPOI,RCMP,RKSP,RPI_A,RSPS,SDPI,SJPA,SP,SUCI,VJCP, \
                WPOI,Grand_Total = row
                region, created = Region.objects.get_or_create(name=region)
                region.state = state_obj
                region.save()
                constituency_obj, created = Constituency.objects.get_or_create(
                    state_id=state_obj.id, name=AC_name, 
                )
                constituency_obj.region = region
                constituency_obj.save()
                parties_name = [
                    "ABHM","ABML(S)","ADMK","AIFB(S)","AKBRP","AMJP","ANC",
                    "BDBRAJP","BhJD","BHPP","BJP","BPJP","BRPP","BSP","BSRCP",
                    "CPI","CPI(ML)(L)","CPIM","CPM","DPPS","HJP","HND","INC",
                    "IND","INL","IUML","JD(S)","JD(U)","JVBP","KAP","KaSP",
                    "KCVP","KDC","KJP","KMP","KRRS","LJP","LSP","MPHP","NCP",
                    "NDEP","NPP","PPIS","PPOI","RCMP","RPI","RPI(A)","RSPS",
                    "SAJP","SDP","SDPI","SHS","SJKP","SJPA","SK","SKP","SP",
                    "SUCI","VJCP","WPOI"
                ]
                parties_name_dict = {
                    "ANC": int(ANC) if  ANC else 0,
                    "BDBRAJP": int(BDBRAJP) if  BDBRAJP else 0,
                    "BHPP": int(BHPP) if  BHPP else 0,
                    "BJP": int(BJP) if  BJP else 0,
                    "BSP": int(BSP) if  BSP else 0,
                    "CPI": int(CPI) if  CPI else 0,
                    "CPI(ML)(L)": int(CPI_ML_L) if  CPI_ML_L else 0,
                    "CPIM": int(CPIM) if  CPIM else 0,
                    "CPM": int(CPM) if  CPM else 0,
                    "DPPS": int(DPPS) if  DPPS else 0,
                    "HJP": int(HJP) if  HJP else 0,
                    "INC": int(INC) if  INC else 0,
                    "IND": int(IND) if  IND else 0,
                    "JD(S)": int(JD_S) if  JD_S else 0,
                    "JD(U)": int(JD_U) if  JD_U else 0,
                    "JVBP": int(JVBP) if  JVBP else 0,
                    "KAP": int(KAP) if  KAP else 0,
                    "KCVP": int(KCVP) if  KCVP else 0,
                    "KDC": int(KDC) if  KDC else 0,
                    "LSP": int(LSP) if  LSP else 0,
                    "NCP": int(NCP) if  NCP else 0,
                    "NDEP": int(NDEP) if  NDEP else 0,
                    "PPOI": int(PPOI) if  PPOI else 0,
                    "RCMP": int(RCMP) if  RCMP else 0,
                    "RPI(A)": int(RPI_A) if  RPI_A else 0,
                    "RSPS": int(RSPS) if  RSPS else 0,
                    "SDPI": int(SDPI) if  SDPI else 0,
                    "SJPA": int(SJPA) if  SJPA else 0,
                    "SP": int(SP) if  SP else 0,
                    "SUCI": int(SUCI) if  SUCI else 0,
                    "VJCP": int(VJCP) if  VJCP else 0,
                    "WPOI": int(WPOI) if  WPOI else 0,
                }
                Party_vote = collections.namedtuple('Party_vote', 'votes party_name')
                best_votes = sorted([Party_vote(v,k) for (k,v) in parties_name_dict.items()], reverse=True)

                for level, party_vote in enumerate(best_votes):
                    party_name = party_vote.party_name
                    votes = party_vote.votes

                    party_obj, created = Party.objects.get_or_create(name=party_name, abbr=party_name)
                    group_obj, created = Group.objects.get_or_create(name=party_name)
                    group_obj.parties.add(party_obj)
                    group_obj.save()
                    election_candidate_objs = ElectionCandidate.objects.filter(
                        election_id=election_2013.id,
                        constituency_id=constituency_obj.id,
                        party_id=party_obj.id
                    )
                    if year==2013:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj.group=group_obj
                            election_candidate_obj.save()
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            # print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                            #     party_obj.name, constituency_obj.name
                            # )
                            continue
                    elif year==2014:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj, created = ElectionCandidate.objects.get_or_create(
                                election_id=election_2014.id,
                                constituency_id=constituency_obj.id,
                                party_id=party_obj.id,
                                candidate_id=election_candidate_obj.candidate.id,
                                group_id=group_obj.id
                            )
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            # print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                            #     party_obj.name, constituency_obj.name
                            # )
                            continue
                    result_vote, created = ResultVote.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        result_vote.level=level + 1
                        win_status = win_status_dict.get(level + 1, 'Lost')
                        result_vote.votes=votes
                        result_vote.status=win_status
                        result_vote.is_result_final=True
                        result_vote.save()
                    result_vote_history, created = ResultVoteHistory.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        win_status = win_status_dict.get(level, 'Lost')
                        result_vote_history.level = level + 1
                        result_vote_history.votes = votes
                        result_vote_history.is_result_final = True
                        result_vote_history.status=win_status
                        result_vote_history.save()
                    election_result_obj, created = ElectionResult.objects.get_or_create(
                        election=election,
                        election_candidate=election_candidate_obj,
                        candidate=candidate_obj,
                        state=state_obj,
                        constituency=constituency_obj,
                        party=party_obj
                    )
                    if votes:
                        election_result_obj.vote=votes
                        election_result_obj.group=party_obj.group_set.all()[0]
                        election_result_obj.save()
            counter += 1
