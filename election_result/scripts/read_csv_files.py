import csv
import collections
from winner.models import Election, Party, Group, State, Constituency, Region
from winner.models import Candidate, ElectionCandidate, UserConstituency, Group
from winner.models import ElectionResult, ResultVote, ResultVoteHistory

parties_name_dict = {
    "ABHM": "ABHM",
    "ABML(S)": "ABML_S",
    "ADMK": "ADMK",
    "AIFB(S)": "AIFB_S",
    "AKBRP": "AKBRP",
    "AMJP": "AMJP",
    "ANC": "ANC",
    "BDBRAJP": "BDBRAJP",
    "BhJD": "BhJD",
    "BHPP": "BHPP",
    "BJP": "BJP",
    "BPJP": "BPJP",
    "BRPP": "BRPP",
    "BSP": "BSP",
    "BSRCP": "BSRCP",
    "CPI": "CPI",
    "CPI(ML)(L)": "CPI_ML_L",
    "CPIM": "CPIM",
    "CPM": "CPM",
    "DPPS": "DPPS",
    "HJP": "HJP",
    "HND": "HND",
    "INC": "INC",
    "IND": "IND",
    "INL": "INL",
    "IUML": "IUML",
    "JD(S)": "JD_S",
    "JD(U)": "JD_U",
    "JVBP": "JVBP",
    "KAP": "KAP",
    "KaSP": "KaSP",
    "KCVP": "KCVP",
    "KDC": "KDC",
    "KJP": "KJP",
    "KMP": "KMP",
    "KRRS": "KRRS",
    "LJP": "LJP",
    "LSP": "LSP",
    "MPHP": "MPHP",
    "NCP": "NCP",
    "NDEP": "NDEP",
    "NPP": "NPP",
    "PPIS": "PPIS",
    "PPOI": "PPOI",
    "RCMP": "RCMP",
    "RPI": "RPI",
    "RPI(A)": "RPI_A",
    "RSPS": "RSPS",
    "SAJP": "SAJP",
    "SDP": "SDP",
    "SDPI": "SDPI",
    "SHS": "SHS",
    "SJKP": "SJKP",
    "SJPA": "SJPA",
    "SK": "SK",
    "SKP": "SKP",
    "SP": "SP",
    "SUCI": "SUCI",
    "VJCP": "VJCP",
    "WPOI": "WPOI",
}
def read_eletors_data():
    with open('LA_2013_Elelctors.csv', "r") as f:
        reader = csv.reader(f)
        counter = 0
        election, created = Election.objects.get_or_create(
            name='General Election to State Legislative ASSemblies 2013'
        )
        for row in reader:
            if counter>3:
                STATECODE, STATE, AC_NO, CONSTITUENCY, MONTH, YEAR, \
                TOTAL_VOTES_POLLED, TOTAL_ELECTORS, POLL_PERCENTAGE = row
                state_obj, created = State.objects.get_or_create(name=STATE)
                if created:
                    state_obj.code=STATECODE
                    state_obj.save()
                constituency_obj, created = Constituency.objects.get_or_create(
                    state_id=state_obj.id, name=CONSTITUENCY
                )
                election_results = ElectionResult.objects.filter(
                    election=election,
                    state=state_obj,
                    constituency=constituency_obj
                )
                if election_results.exists():
                    election_result = election_results[0]
                    percent = (election_result.vote/int(TOTAL_VOTES_POLLED)) * 100
                    election_result.percent = percent
                    election_result.save()
            counter += 1

def read_candidate_data():
    with open('LA_2013_Candidates.csv', "rb") as f:
        reader = csv.reader(f)
        counter = 0
        election, created = Election.objects.get_or_create(
            name='General Election to State Legislative ASSemblies 2013'
        )
        for row in reader:
            if counter>4:
                ST_CODE, ST_NAME, MONTH, YEAR, DIST_NAME, AC_NO, AC_NAME, \
                AC_TYPE, CAND_NAME, CAND_SEX, CAND_CATEGORY, CAND_AGE, \
                PARTY_Abbreviation, Total_valid_votes_polled, POSITION = row
                candidate_obj, created = Candidate.objects.get_or_create(
                    name=CAND_NAME, gender=CAND_SEX, age=CAND_AGE
                )
                state_obj, created = State.objects.get_or_create(name=ST_NAME)
                if created:
                    state_obj.code = ST_CODE
                    state_obj.save()
                constituency_obj, created = Constituency.objects.get_or_create(
                    state_id=state_obj.id, name=AC_NAME
                )
                party, created = Party.objects.get_or_create(
                    name=PARTY_Abbreviation, abbr=PARTY_Abbreviation
                )
                election_candidate_obj, created = ElectionCandidate.objects.get_or_create(
                    election_id=election.id,
                    candidate_id=candidate_obj.id,
                    constituency_id=constituency_obj.id,
                    party_id=party.id
                )
                result_vote, created = ResultVote.objects.get_or_create(
                    user_id=1,
                    election=election,
                    candidate=candidate_obj,
                    constituency=constituency_obj,
                    state=state_obj,
                )
                result_vote.level = POSITION
                result_vote.votes = Total_valid_votes_polled
                result_vote.is_result_final = False
                if POSITION in [1, '1']:
                    result_vote.status='L'
                result_vote.save()
                result_vote_history, created = ResultVoteHistory.objects.get_or_create(
                    user_id=1,
                    election=election,
                    candidate=candidate_obj,
                    constituency=constituency_obj,
                    state=state_obj,
                )
                result_vote_history.level = POSITION
                result_vote_history.votes = Total_valid_votes_polled
                result_vote_history.is_result_final = False
                if POSITION in [1, '1']:
                    result_vote_history.status='L'
                    election_result_obj, created = ElectionResult.objects.get_or_create(
                        election=election,
                        election_candidate=election_candidate_obj,
                        candidate=candidate_obj,
                        state=state_obj,
                        constituency=constituency_obj,
                        party=party
                    )
                    election_result_obj.vote=Total_valid_votes_polled
                    election_result_obj.save()
                result_vote_history.save()
            counter += 1

def create_parties():
    parties_name = [
        "ABHM","ABML(S)","ADMK","AIFB(S)","AKBRP","AMJP","ANC",
        "BDBRAJP","BhJD","BHPP","BJP","BPJP","BRPP","BSP","BSRCP",
        "CPI","CPI(ML)(L)","CPIM","CPM","DPPS","HJP","HND","INC",
        "IND","INL","IUML","JD(S)","JD(U)","JVBP","KAP","KaSP",
        "KCVP","KDC","KJP","KMP","KRRS","LJP","LSP","MPHP","NCP",
        "NDEP","NPP","PPIS","PPOI","RCMP","RPI","RPI(A)","RSPS",
        "SAJP","SDP","SDPI","SHS","SJKP","SJPA","SK","SKP","SP",
        "SUCI","VJCP","WPOI"
    ]
    for party_name in parties_name:
        parties_obj, created = Party.objects.get_or_create(name=parties_name, abbr=parties_name)
        group_obj, created = Group.objects.get_or_create(name=parties_name)
        group_obj.parties.add(parties_obj)
        group_obj.save()

def read_karnataka_result_compare(year):
    win_status_dict = {
        1: 'L',
        2: 'RunnerUp',
        3: 'SecondRunnerUp',
    }
    election_2013, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2013'
    )
    election_2014, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2014'
    )
    if year==2013:
        election = election_2013
    if year==2014:
        election = election_2014
    state_obj, created = State.objects.get_or_create(name='Karnataka')
    file_name = 'Karnataka_13_14_results_compare_{0}.csv'.format(year)
    with open(file_name, "rb") as f:
        reader = csv.reader(f)
        counter = 0
        for row in reader:
            if counter>0:
                AC_NO,AC_name,region,WIN,RUP,SRUP,WIN_votes,RUP_votes, \
                SRUP_votes,mov,pmov,Total,ABHM,ABML_S,ADMK,AIFB_S,AKBRP, \
                AMJP,ANC,BDBRAJP,BhJD,BHPP,BJP,BPJP,BRPP,BSP,BSRCP,CPI, \
                CPI_ML_L,CPIM,CPM,DPPS,HJP,HND,INC,IND,INL,IUML,JD_S, \
                JD_U,JVBP,KAP,KaSP,KCVP,KDC,KJP,KMP,KRRS,LJP,LSP,MPHP,NCP, \
                NDEP,NPP,PPIS,PPOI,RCMP,RPI,RPI_A,RSPS,SAJP,SDP,SDPI,SHS, \
                SJKP,SJPA,SK,SKP,SP,SUCI,VJCP,WPOI,Grand_Total = row
                region, created = Region.objects.get_or_create(name=region)
                region.state = state_obj
                region.save()
                constituency_obj, created = Constituency.objects.get_or_create(
                    state_id=state_obj.id, name=AC_name, 
                )
                constituency_obj.region = region
                constituency_obj.save()
                parties_name = [
                    "ABHM","ABML(S)","ADMK","AIFB(S)","AKBRP","AMJP","ANC",
                    "BDBRAJP","BhJD","BHPP","BJP","BPJP","BRPP","BSP","BSRCP",
                    "CPI","CPI(ML)(L)","CPIM","CPM","DPPS","HJP","HND","INC",
                    "IND","INL","IUML","JD(S)","JD(U)","JVBP","KAP","KaSP",
                    "KCVP","KDC","KJP","KMP","KRRS","LJP","LSP","MPHP","NCP",
                    "NDEP","NPP","PPIS","PPOI","RCMP","RPI","RPI(A)","RSPS",
                    "SAJP","SDP","SDPI","SHS","SJKP","SJPA","SK","SKP","SP",
                    "SUCI","VJCP","WPOI"
                ]
                parties_name_dict = {
                    "ABHM": int(ABHM) if ABHM else 0,
                    "ABML(S)": int(ABML_S) if ABML_S else 0,
                    "ADMK": int(ADMK) if ADMK else 0,
                    "AIFB(S)": int(AIFB_S) if AIFB_S else 0,
                    "AKBRP": int(AKBRP) if AKBRP else 0,
                    "AMJP": int(AMJP) if AMJP else 0,
                    "ANC": int(ANC) if ANC else 0,
                    "BDBRAJP": int(BDBRAJP) if BDBRAJP else 0,
                    "BhJD": int(BhJD) if BhJD else 0,
                    "BHPP": int(BHPP) if BHPP else 0,
                    "BJP": int(BJP) if BJP else 0,
                    "BPJP": int(BPJP) if BPJP else 0,
                    "BRPP": int(BRPP) if BRPP else 0,
                    "BSP": int(BSP) if BSP else 0,
                    "BSRCP": int(BSRCP) if BSRCP else 0,
                    "CPI": int(CPI) if CPI else 0,
                    "CPI(ML)(L)": int(CPI_ML_L) if CPI_ML_L else 0,
                    "CPIM": int(CPIM) if CPIM else 0,
                    "CPM": int(CPM) if CPM else 0,
                    "DPPS": int(DPPS) if DPPS else 0,
                    "HJP": int(HJP) if HJP else 0,
                    "HND": int(HND) if HND else 0,
                    "INC": int(INC) if INC else 0,
                    "IND": int(IND) if IND else 0,
                    "INL": int(INL) if INL else 0,
                    "IUML": int(IUML) if IUML else 0,
                    "JD(S)": int(JD_S) if JD_S else 0,
                    "JD(U)": int(JD_U) if JD_U else 0,
                    "JVBP": int(JVBP) if JVBP else 0,
                    "KAP": int(KAP) if KAP else 0,
                    "KaSP": int(KaSP) if KaSP else 0,
                    "KCVP": int(KCVP) if KCVP else 0,
                    "KDC": int(KDC) if KDC else 0,
                    "KJP": int(KJP) if KJP else 0,
                    "KMP": int(KMP) if KMP else 0,
                    "KRRS": int(KRRS) if KRRS else 0,
                    "LJP": int(LJP) if LJP else 0,
                    "LSP": int(LSP) if LSP else 0,
                    "MPHP": int(MPHP) if MPHP else 0,
                    "NCP": int(NCP) if NCP else 0,
                    "NDEP": int(NDEP) if NDEP else 0,
                    "NPP": int(NPP) if NPP else 0,
                    "PPIS": int(PPIS) if PPIS else 0,
                    "PPOI": int(PPOI) if PPOI else 0,
                    "RCMP": int(RCMP) if RCMP else 0,
                    "RPI": int(RPI) if RPI else 0,
                    "RPI(A)": int(RPI_A) if RPI_A else 0,
                    "RSPS": int(RSPS) if RSPS else 0,
                    "SAJP": int(SAJP) if SAJP else 0,
                    "SDP": int(SDP) if SDP else 0,
                    "SDPI": int(SDPI) if SDPI else 0,
                    "SHS": int(SHS) if SHS else 0,
                    "SJKP": int(SJKP) if SJKP else 0,
                    "SJPA": int(SJPA) if SJPA else 0,
                    "SK": int(SK) if SK else 0,
                    "SKP": int(SKP) if SKP else 0,
                    "SP": int(SP) if SP else 0,
                    "SUCI": int(SUCI) if SUCI else 0,
                    "VJCP": int(VJCP) if VJCP else 0,
                    "WPOI": int(WPOI) if WPOI else 0,
                }
                Party_vote = collections.namedtuple('Party_vote', 'votes party_name')
                best_votes = sorted([Party_vote(v,k) for (k,v) in parties_name_dict.items()], reverse=True)
                for level, party_vote in enumerate(best_votes):
                    party_name = party_vote.party_name
                    votes = party_vote.votes
                    party_obj, created = Party.objects.get_or_create(name=party_name, abbr=party_name)
                    group_obj, created = Group.objects.get_or_create(name=party_name)
                    group_obj.parties.add(party_obj)
                    group_obj.save()
                    election_candidate_objs = ElectionCandidate.objects.filter(
                        election_id=election_2013.id,
                        constituency_id=constituency_obj.id,
                        party_id=party_obj.id
                    )
                    if year==2013:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj.group=group_obj
                            election_candidate_obj.save()
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                                party_obj.name, constituency_obj.name
                            )
                            continue
                    elif year==2014:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj, created = ElectionCandidate.objects.get_or_create(
                                election_id=election_2014.id,
                                constituency_id=constituency_obj.id,
                                party_id=party_obj.id,
                                candidate_id=election_candidate_obj.candidate.id,
                                group_id=group_obj.id
                            )
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                                party_obj.name, constituency_obj.name
                            )
                            continue
                    result_vote, created = ResultVote.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        result_vote.level=level + 1
                        win_status = win_status_dict.get(level + 1, 'Lost')
                        result_vote.votes=votes
                        result_vote.status=win_status
                        result_vote.is_result_final=True
                        result_vote.save()
                    result_vote_history, created = ResultVoteHistory.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        win_status = win_status_dict.get(level + 1, 'Lost')
                        result_vote_history.level = level + 1
                        result_vote_history.votes = votes
                        result_vote_history.is_result_final = True
                        result_vote_history.status=win_status
                        result_vote_history.save()
                    election_result_obj, created = ElectionResult.objects.get_or_create(
                        election=election,
                        election_candidate=election_candidate_obj,
                        candidate=candidate_obj,
                        state=state_obj,
                        constituency=constituency_obj,
                        party=party_obj
                    )
                    if votes:
                        election_result_obj.vote=votes
                        election_result_obj.group=party_obj.group_set.all()[0]
                        election_result_obj.save()
            counter += 1
def read_karnataka_result_compare_2014(year):
    win_status_dict = {
        1: 'L',
        2: 'RunnerUp',
        3: 'SecondRunnerUp',
    }
    election_2013, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2013'
    )
    election_2014, created = Election.objects.get_or_create(
        name='General Election to State Legislative ASSemblies 2014'
    )
    if year==2013:
        election = election_2013
    if year==2014:
        election = election_2014
    state_obj, created = State.objects.get_or_create(name='Karnataka')
    file_name = 'Karnataka_13_14_results_compare_{0}.csv'.format(year)
    with open(file_name, "rb") as f:
        reader = csv.reader(f)
        counter = 0
        bjp_counter = 0
        for row in reader:
            if counter>0:
                Ac_No,AC_name,region,WIN,RUP,SRUP,WIN_VOTES,RUP_VOTES, \
                SRUP_VOTES,MOV,PMOV,Total,AAAP,AIFB,AITC,ANC,asdp, \
                BDBRAJP,BHPP,BJP,BMUP,BSP,CPI,CPI_ML_L,CPIM,CPM,DPPS, \
                GaAP,HJP,INC,IND,JD_S,JD_U,JVBP,KAP,KCVP,KDC,LSP,NCP, \
                NDEP,PPOI,RCMP,RKSP,RPI_A,RSPS,SDPI,SJPA,SP,SUCI,VJCP, \
                WPOI,Grand_Total = row
                region, created = Region.objects.get_or_create(name=region)
                region.state = state_obj
                region.save()
                constituency_obj, created = Constituency.objects.get_or_create(
                    state_id=state_obj.id, name=AC_name, 
                )
                constituency_obj.region = region
                constituency_obj.save()
                parties_name = [
                    "ABHM","ABML(S)","ADMK","AIFB(S)","AKBRP","AMJP","ANC",
                    "BDBRAJP","BhJD","BHPP","BJP","BPJP","BRPP","BSP","BSRCP",
                    "CPI","CPI(ML)(L)","CPIM","CPM","DPPS","HJP","HND","INC",
                    "IND","INL","IUML","JD(S)","JD(U)","JVBP","KAP","KaSP",
                    "KCVP","KDC","KJP","KMP","KRRS","LJP","LSP","MPHP","NCP",
                    "NDEP","NPP","PPIS","PPOI","RCMP","RPI","RPI(A)","RSPS",
                    "SAJP","SDP","SDPI","SHS","SJKP","SJPA","SK","SKP","SP",
                    "SUCI","VJCP","WPOI"
                ]
                parties_name_dict = {
                    "ANC": int(ANC) if  ANC else 0,
                    "BDBRAJP": int(BDBRAJP) if  BDBRAJP else 0,
                    "BHPP": int(BHPP) if  BHPP else 0,
                    "BJP": int(BJP) if  BJP else 0,
                    "BSP": int(BSP) if  BSP else 0,
                    "CPI": int(CPI) if  CPI else 0,
                    "CPI(ML)(L)": int(CPI_ML_L) if  CPI_ML_L else 0,
                    "CPIM": int(CPIM) if  CPIM else 0,
                    "CPM": int(CPM) if  CPM else 0,
                    "DPPS": int(DPPS) if  DPPS else 0,
                    "HJP": int(HJP) if  HJP else 0,
                    "INC": int(INC) if  INC else 0,
                    "IND": int(IND) if  IND else 0,
                    "JD(S)": int(JD_S) if  JD_S else 0,
                    "JD(U)": int(JD_U) if  JD_U else 0,
                    "JVBP": int(JVBP) if  JVBP else 0,
                    "KAP": int(KAP) if  KAP else 0,
                    "KCVP": int(KCVP) if  KCVP else 0,
                    "KDC": int(KDC) if  KDC else 0,
                    "LSP": int(LSP) if  LSP else 0,
                    "NCP": int(NCP) if  NCP else 0,
                    "NDEP": int(NDEP) if  NDEP else 0,
                    "PPOI": int(PPOI) if  PPOI else 0,
                    "RCMP": int(RCMP) if  RCMP else 0,
                    "RPI(A)": int(RPI_A) if  RPI_A else 0,
                    "RSPS": int(RSPS) if  RSPS else 0,
                    "SDPI": int(SDPI) if  SDPI else 0,
                    "SJPA": int(SJPA) if  SJPA else 0,
                    "SP": int(SP) if  SP else 0,
                    "SUCI": int(SUCI) if  SUCI else 0,
                    "VJCP": int(VJCP) if  VJCP else 0,
                    "WPOI": int(WPOI) if  WPOI else 0,
                }
                Party_vote = collections.namedtuple('Party_vote', 'votes party_name')
                best_votes = sorted([Party_vote(v,k) for (k,v) in parties_name_dict.items()], reverse=True)

                for level, party_vote in enumerate(best_votes):
                    party_name = party_vote.party_name
                    votes = party_vote.votes

                    party_obj, created = Party.objects.get_or_create(name=party_name, abbr=party_name)
                    group_obj, created = Group.objects.get_or_create(name=party_name)
                    group_obj.parties.add(party_obj)
                    group_obj.save()
                    election_candidate_objs = ElectionCandidate.objects.filter(
                        election_id=election_2013.id,
                        constituency_id=constituency_obj.id,
                        party_id=party_obj.id
                    )
                    if year==2013:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj.group=group_obj
                            election_candidate_obj.save()
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            # print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                            #     party_obj.name, constituency_obj.name
                            # )
                            continue
                    elif year==2014:
                        if election_candidate_objs.exists():
                            election_candidate_obj = election_candidate_objs[0]
                            election_candidate_obj, created = ElectionCandidate.objects.get_or_create(
                                election_id=election_2014.id,
                                constituency_id=constituency_obj.id,
                                party_id=party_obj.id,
                                candidate_id=election_candidate_obj.candidate.id,
                                group_id=group_obj.id
                            )
                            candidate_obj = election_candidate_obj.candidate
                        else:
                            # print "error: no election_candidate_obj for party {0} in constituency {1}".format(
                            #     party_obj.name, constituency_obj.name
                            # )
                            continue
                    result_vote, created = ResultVote.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        result_vote.level=level + 1
                        win_status = win_status_dict.get(level + 1, 'Lost')
                        result_vote.votes=votes
                        result_vote.status=win_status
                        result_vote.is_result_final=True
                        result_vote.save()
                    result_vote_history, created = ResultVoteHistory.objects.get_or_create(
                        user_id=1,
                        election=election,
                        candidate=candidate_obj,
                        constituency=constituency_obj,
                        state=state_obj,
                    )
                    if votes:
                        win_status = win_status_dict.get(level, 'Lost')
                        result_vote_history.level = level + 1
                        result_vote_history.votes = votes
                        result_vote_history.is_result_final = True
                        result_vote_history.status=win_status
                        result_vote_history.save()
                    election_result_obj, created = ElectionResult.objects.get_or_create(
                        election=election,
                        election_candidate=election_candidate_obj,
                        candidate=candidate_obj,
                        state=state_obj,
                        constituency=constituency_obj,
                        party=party_obj
                    )
                    if votes:
                        election_result_obj.vote=votes
                        election_result_obj.group=party_obj.group_set.all()[0]
                        election_result_obj.save()
            counter += 1
