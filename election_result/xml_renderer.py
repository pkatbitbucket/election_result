from rest_framework_xml.renderers import XMLRenderer

class ModifiedXMLRenderer(XMLRenderer):
    item_tag_name = "Table"
    root_tag_name = "NewDataSet"