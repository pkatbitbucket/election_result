# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import User
from winner.models import Party, State, Constituency, Candidate
from winner.models import UserConstituency, UserProfile, ElectionCandidate
from winner.models import Region, Group, Election, ResultVote, ResultVoteHistory, ElectionResult
from winner.adminform import UserProfileForm


class ElectionAdmin(admin.ModelAdmin):
    list_display = [field.attname for field in Election._meta.fields]


class ElectionCandidateAdmin(admin.ModelAdmin):
    list_display = ('election', 'candidate', 'code', 'constituency', 'party', 'group', 'is_vip', 'phase', 'criminal_cases', 'assets', 'created', 'updated',)
    list_filter = ('election', 'party', 'group', 'constituency',)


class GroupAdmin(admin.ModelAdmin):
    list_display = [field.attname for field in Group._meta.fields]


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'state', 'created', 'updated',)


class PartyAdmin(admin.ModelAdmin):
    list_display = [field.attname for field in Party._meta.fields]


class StateAdmin(admin.ModelAdmin):
    list_display = [field.attname for field in State._meta.fields]


class ConstituencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'state', 'region', 'code', 'created', 'updated',)
    list_filter = ('state', 'region',)


class CandidateAdmin(admin.ModelAdmin):
    list_display = [field.attname for field in Candidate._meta.fields]


class UserProfileAdmin(admin.ModelAdmin):
    list_display = [field.attname for field in UserProfile._meta.fields]


class UserProfileAdmin(admin.ModelAdmin):
    search_fields = ['mobile', 'first_name', 'last_name']
    list_display = [field.attname for field in UserProfile._meta.fields]
    form = UserProfileForm

    def save_model(self, request, obj, form, change):
        data = form.clean()
        user, created = User.objects.get_or_create(username=data['mobile'])
        if data.get('name'):
            user.first_name = data.get('name').split(' ')[0]
        if data.get('name') and len(data.get('name').split(' ')) > 1:
            user.last_name = ' '.join(data.get('name').split(' ')[1:])
        if data.get('password'):
            user.set_password(data.get('password'))
        user.save()
        obj.user = user
        obj.mobile = data['mobile']
        obj.name = data['name']
        obj.save()
        return obj


class ElectionResultAdmin(admin.ModelAdmin):
    list_filter = ['state', 'region', 'constituency', 'party']
    list_display = [
        'candidate', 'group', 'state', 'region', 'constituency', 'party',
        'status', 'vote', 'percent'
    ]


class UserConstituencyAdmin(admin.ModelAdmin):
    list_display = ('user', 'election', 'constituency', 'created', 'updated',)
    list_filter = ('election', 'constituency',)


class ResultVoteAdmin(admin.ModelAdmin):
    list_display = ('election', 'state', 'constituency', 'level', 'status',)
    list_filter = ('election', 'state', 'constituency', 'level', 'status',)


class ResultVoteHistoryAdmin(admin.ModelAdmin):
    list_display = ('election', 'candidate', 'constituency', 'state', 'level', 'status', 'votes', 'uuid', 'created',)
    list_filter = ('election', 'state', 'constituency', 'level', 'status',)


admin.site.register(ElectionResult, ElectionResultAdmin)
admin.site.register(Election, ElectionAdmin)
admin.site.register(ElectionCandidate, ElectionCandidateAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Party, PartyAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Constituency, ConstituencyAdmin)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(UserConstituency, UserConstituencyAdmin)
admin.site.register(ResultVote, ResultVoteAdmin)
admin.site.register(ResultVoteHistory, ResultVoteHistoryAdmin)
