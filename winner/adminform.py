__author__ = 'pramod'
import datetime
from django import forms
from django.contrib.admin.helpers import ActionForm
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from winner.models import UserProfile


class UserProfileForm(forms.ModelForm):
    password = forms.CharField(
        max_length=128, required=False, widget=forms.PasswordInput
    )
    class Meta:
        model = UserProfile
        fields = [
            'name',
            'mobile',
            'password',
        ]