# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import textwrap
import uuid
from django.contrib.auth.models import User
from django.db.models import F, Sum, FloatField, Avg, Max, Q
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic.base import View
from rest_framework import viewsets, generics
from rest_framework import viewsets, generics, status, mixins
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_xml.renderers import XMLRenderer
from winner.models import Party, State, Constituency, Candidate
from winner.models import UserProfile, ResultVote, Election, Region
from winner.models import *
from winner.serializers import PartySerializer, StateSerializer
from winner.serializers import ConstituencySerializer, CandidateSerializer
from winner.serializers import UserProfileSerializer, ResultVoteSerializer, UserProfileSerializerNormal
from winner.serializers import ElectionDataSerializer
from winner.serializers import ResultVoteHistoryPostSerializer

# <Table>
#     <state_name>Gujarat</state_name>
#     <state_id>102</state_id>
#     <party_name>BJP</party_name>
#     <party_id>2</party_id>
#     <group_name>BJP</group_name>
#     <group_id>2</group_id>
#     <leading>0</leading>
#     <won>99</won>
#     <total>99</total>
#     <last_won>115</last_won>
#     <gain_loss>-16</gain_loss>
#     <party_color_code>#f78002</party_color_code>
#     <party_symbol_path>http://timesofindia.indiatimes.com/photo/51774624.cms</party_symbol_path>
#     <Total_Seats>182</Total_Seats>
#     <ind_party_color>#f78002</ind_party_color>
#     <ind_party_symbol_path>http://timesofindia.indiatimes.com/photo/51774624.cms</ind_party_symbol_path>
#   </Table>

class AllyGainLoseXmlViewSet(viewsets.ModelViewSet):
    queryset = ResultVote.objects.all()
    serializer_class = ResultVoteSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        election_id = request.GET.get('election_id')
        old_election_id = request.GET.get('old_election_id')
        if (not old_election_id) or (not election_id):
            election_id = 3
            old_election_id = 2
            # return Response(
            #     {'error': 'election_id and old_election_id is mandetory'},
            #     status=status.HTTP_400_BAD_REQUEST
            # )
        elections = Election.objects.filter(id=election_id)
        election = None
        if elections.exists():
            election = elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        old_election = None
        old_elections = Election.objects.filter(id=old_election_id)
        if old_elections.exists():
            old_election = old_elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        election_candidates = election.electioncandidate_set.all()
        old_election_candidates = old_election.electioncandidate_set.all()
        parties = election_candidates.values_list('party', flat=True).distinct()
        constituencies = election_candidates.values_list('constituency', flat=True).distinct()
        states = election_candidates.values_list('constituency__state_id', flat=True).distinct()
        ally_gain_lose_data_list = []
        for state in states:
            for party in parties:
                ally_gain_lose_data_dict = dict()
                state_obj = State.objects.get(id=state)
                ally_gain_lose_data_dict['state_name'] = state_obj.name
                ally_gain_lose_data_dict['state_id'] = state_obj.id
                party_obj = Party.objects.get(id=party)
                ally_gain_lose_data_dict['party_name'] = party_obj.name
                ally_gain_lose_data_dict['party_id'] = party_obj.id
                ally_gain_lose_data_dict['party_color_code'] = party_obj.code
                ally_gain_lose_data_dict['party_symbol_path'] = party_obj.logo
                ally_gain_lose_data_dict['Total_Seats'] = Constituency.objects.filter(
                    id__in=constituencies, state_id=state
                ).count()
                votes_results = ResultVote.objects.filter(
                    election_id=election,
                    state_id=state,
                    candidate_id__in=election_candidates.values_list('candidate', flat=True).filter(party_id=party),
                )
                old_votes_results = ResultVote.objects.filter(
                    election_id=old_election,
                    state_id=state,
                    candidate_id__in=old_election_candidates.values_list('candidate', flat=True).filter(party_id=party),
                )
                last_won_count = old_votes_results.filter(is_result_final=True, status='L').count()
                won_count = votes_results.filter(is_result_final=True, status='L').count()
                leading_count = votes_results.filter(status='L', is_result_final=False).count()
                ally_gain_lose_data_dict['last_won'] = last_won_count
                ally_gain_lose_data_dict['gain_loss'] = won_count - last_won_count
                ally_gain_lose_data_dict['leading'] = leading_count
                ally_gain_lose_data_dict['won'] = won_count
                ally_gain_lose_data_dict['total'] = won_count + leading_count
                group_name = None
                group_id = None
                if party_obj.group_set.all().exists():
                    group = party_obj.group_set.all()[0]
                    group_id = group.id
                    group_name = group.name
                ally_gain_lose_data_dict['group_name'] = group_name
                ally_gain_lose_data_dict['group_id'] = group_id
                ally_gain_lose_data_dict['ind_party_symbol_path'] = 'static data'
                ally_gain_lose_data_dict['ind_party_color'] = 'static data'
                ally_gain_lose_data_list.append(ally_gain_lose_data_dict)
        return Response(ally_gain_lose_data_list, status=status.HTTP_200_OK)

class AllyGainLoseRegionXmlViewSet(viewsets.ModelViewSet):
    queryset = ResultVote.objects.all()
    serializer_class = ResultVoteSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        election_id = request.GET.get('election_id')
        old_election_id = request.GET.get('old_election_id')
        if (not old_election_id) or (not election_id):
            election_id = 3
            old_election_id = 2
            # return Response(
            #     {'error': 'election_id and old_election_id is mandetory'},
            #     status=status.HTTP_400_BAD_REQUEST
            # )
        elections = Election.objects.filter(id=election_id)
        election = None
        if elections.exists():
            election = elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        old_election = None
        old_elections = Election.objects.filter(id=old_election_id)
        if old_elections.exists():
            old_election = old_elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        election_candidates = election.electioncandidate_set.all()
        old_election_candidates = old_election.electioncandidate_set.all()
        parties = election_candidates.values_list('party', flat=True).distinct()
        constituencies = election_candidates.values_list('constituency', flat=True).distinct()
        regions = election_candidates.values_list('constituency__region_id', flat=True).distinct()
        ally_gain_lose_data_list = []
        for region in regions:
            for party in parties:
                ally_gain_lose_data_dict = dict()
                region_obj = Region.objects.get(id=region)
                ally_gain_lose_data_dict['region'] = region_obj.name
                ally_gain_lose_data_dict['region_id'] = region_obj.id
                ally_gain_lose_data_dict['state_name'] = region_obj.state.name
                ally_gain_lose_data_dict['state_id'] = region_obj.state.id
                party_obj = Party.objects.get(id=party)
                ally_gain_lose_data_dict['party_name'] = party_obj.name
                ally_gain_lose_data_dict['party_id'] = party_obj.id
                ally_gain_lose_data_dict['party_color_code'] = party_obj.code
                ally_gain_lose_data_dict['party_symbol_path'] = party_obj.logo
                ally_gain_lose_data_dict['Total_Seats_region'] = region_obj.constituency_set.all().count()
                votes_results = ResultVote.objects.filter(
                    election_id=election,
                    constituency__region_id=region_obj.id,
                    candidate_id__in=election_candidates.values_list('candidate', flat=True).filter(party_id=party),
                )
                old_votes_results = ResultVote.objects.filter(
                    election_id=old_election,
                    constituency__region_id=region_obj.id,
                    candidate_id__in=old_election_candidates.values_list('candidate', flat=True).filter(party_id=party),
                )
                last_won_count = old_votes_results.filter(is_result_final=True, status='L').count()
                won_count = votes_results.filter(is_result_final=True, status='L').count()
                leading_count = votes_results.filter(status='L', is_result_final=False).count()
                ally_gain_lose_data_dict['last_won'] = last_won_count
                ally_gain_lose_data_dict['gain_loss'] = won_count - last_won_count
                ally_gain_lose_data_dict['leading'] = leading_count
                ally_gain_lose_data_dict['won'] = won_count
                ally_gain_lose_data_dict['total'] = won_count + leading_count
                group_name = None
                group_id = None
                if party_obj.group_set.all().exists():
                    group = party_obj.group_set.all()[0]
                    group_id = group.id
                    group_name = group.name
                ally_gain_lose_data_dict['group_name'] = group_name
                ally_gain_lose_data_dict['group_id'] = group_id
                ally_gain_lose_data_dict['ind_party_symbol_path'] = 'static data'
                ally_gain_lose_data_dict['ind_party_color'] = 'static data'
                ally_gain_lose_data_list.append(ally_gain_lose_data_dict)
        return Response(ally_gain_lose_data_list, status=status.HTTP_200_OK)


"""
  <Table>
    <state_name>Gujarat</state_name>
    <state_id>102</state_id>
    <party_name>CONG</party_name>
    <party_id>1</party_id>
    <group_name>CONG+</group_name>
    <group_id>1</group_id>
    <region>SAURASHTRA</region>
    <region_id>66</region_id>
    <leading>0</leading>
    <won>30</won>
    <total>30</total>
    <last_won>16</last_won>
    <gain_loss>14</gain_loss>
    <party_color_code>#127f32</party_color_code>
    <party_symbol_path>http://timesofindia.indiatimes.com/photo/51774625.cms</party_symbol_path>
    <ind_party_color>#127f32</ind_party_color>
    <ind_party_symbol_path>http://timesofindia.indiatimes.com/photo/51774625.cms</ind_party_symbol_path>
    <Total_Seats_region>54</Total_Seats_region>
  </Table>
"""
class AllyVoteShareXmlViewSet(viewsets.ModelViewSet):
    queryset = ResultVote.objects.all()
    serializer_class = ResultVoteSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        election_id = request.GET.get('election_id')
        old_election_id = request.GET.get('old_election_id')
        if (not old_election_id) or (not election_id):
            election_id = 3
            old_election_id = 2
            # return Response(
            #     {'error': 'election_id and old_election_id is mandetory'},
            #     status=status.HTTP_400_BAD_REQUEST
            # )
        elections = Election.objects.filter(id=election_id)
        election = None
        if elections.exists():
            election = elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        old_election = None
        old_elections = Election.objects.filter(id=old_election_id)
        if old_elections.exists():
            old_election = old_elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        election_candidates = election.electioncandidate_set.all()
        old_election_candidates = old_election.electioncandidate_set.all()
        parties = election_candidates.values_list('party', flat=True).distinct()
        groups = Group.objects.filter(parties__in=parties)
        constituencies = election_candidates.values_list('constituency', flat=True).distinct()
        states = election_candidates.values_list('constituency__state_id', flat=True).distinct()
        ally_gain_lose_data_list = []
        for state in states:
            result_total_votes = ResultVote.objects.filter(
                state_id=state, election_id=election.id
            ).aggregate(total_votes=Sum('votes'))
            total_votes = result_total_votes.get('total_votes')
            old_result_total_votes = ResultVote.objects.filter(
                state_id=state, election_id=old_election.id
            ).aggregate(total_votes=Sum('votes'))
            old_total_votes = old_result_total_votes.get('total_votes')
            for group in groups:
                for party in group.parties.all():
                    ally_gain_lose_data_dict = dict()
                    state_obj = State.objects.get(id=state)
                    ally_gain_lose_data_dict['state_name'] = state_obj.name
                    ally_gain_lose_data_dict['state_id'] = state_obj.id
                    # party_obj = Party.objects.get(id=party)
                    ally_gain_lose_data_dict['party_name'] = party.name
                    ally_gain_lose_data_dict['party_id'] = party.id
                    ally_gain_lose_data_dict['party_color_code'] = party.code
                    ally_gain_lose_data_dict['party_symbol_path'] = party.logo
                    ally_gain_lose_data_dict['ind_party_symbol_path'] = 'static data'
                    ally_gain_lose_data_dict['ind_party_color'] = 'static data'
                    party_candidates = ElectionCandidate.objects.filter(
                        election_id=election.id, party_id=party.id
                    ).values_list('candidate', flat=True).all()
                    result_party_votes = ResultVote.objects.filter(
                        state_id=state, election_id=election.id,
                        candidate_id__in=party_candidates
                    ).aggregate(total_votes=Sum('votes'))
                    new_result_party_votes = result_party_votes.get('total_votes')
                    vote_share = 0
                    if new_result_party_votes and total_votes:
                        vote_share = (float(new_result_party_votes) / float(total_votes))*100
                    old_election_party_candidates = ElectionCandidate.objects.filter(
                        election_id=old_election.id, party_id=party.id
                    ).values_list('candidate', flat=True).all()
                    old_result_party_votes = ResultVote.objects.filter(
                        state_id=state, election_id=old_election.id,
                        candidate_id__in=old_election_party_candidates
                    ).aggregate(total_votes=Sum('votes'))
                    old_result_party_votes_data = old_result_party_votes.get('total_votes')
                    l_vote_share = 0
                    if old_result_party_votes_data and old_total_votes:
                        l_vote_share = (float(old_result_party_votes_data) / float(old_total_votes))*100
                    ally_gain_lose_data_dict['lvote_share'] = l_vote_share
                    ally_gain_lose_data_dict['vote_share'] = vote_share
                    ally_gain_lose_data_list.append(ally_gain_lose_data_dict)
        return Response(ally_gain_lose_data_list, status=status.HTTP_200_OK)
        """
          <Table>
    <state_id>102</state_id>
    <state_name>Gujarat</state_name>
    <group_id>1</group_id>
    <group_name>CONG+</group_name>
    <party_id>1</party_id>
    <party_name>CONG</party_name>
    <vote_share>42.8</vote_share>
    <lvote_share>38.9</lvote_share>
    <party_color_code>#127f32</party_color_code>
    <party_symbol_path>http://timesofindia.indiatimes.com/photo/51774625.cms</party_symbol_path>
    <ind_party_color>#127f32</ind_party_color>
    <ind_party_symbol_path>http://timesofindia.indiatimes.com/photo/51774625.cms</ind_party_symbol_path>
  </Table>
        """
class CandidateViewset(viewsets.ModelViewSet):
    queryset = ResultVote.objects.all()
    serializer_class = ResultVoteSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        state_id = request.GET.get('state_id')
        election_id = request.GET.get('election_id')
        old_election_id = request.GET.get('old_election_id')
        if (not old_election_id) or (not election_id):
            election_id = 3
            old_election_id = 2
            # return Response(
            #     {'error': 'election_id and old_election_id is mandetory'},
            #     status=status.HTTP_400_BAD_REQUEST
            # )
        elections = Election.objects.filter(id=election_id)
        election = None
        if elections.exists():
            election = elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        old_election = None
        old_elections = Election.objects.filter(id=old_election_id)
        if old_elections.exists():
            old_election = old_elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        election_candidates = election.electioncandidate_set.all()
        election_candidate_data = []
        if state_id:
            election_candidates = election_candidates.filter(constituency__state_id=state_id)
        for candidate in election_candidates:
            Total_Seats = Constituency.objects.filter(
                state_id=candidate.constituency.state.id
            ).count()
            election_candidate_data_dict = dict()
            election_candidate_data_dict['state_id'] = candidate.constituency.state.id
            election_candidate_data_dict['state_name'] = candidate.constituency.state.name
            election_candidate_data_dict['region_id'] = candidate.constituency.region.id if candidate.constituency.region else None
            election_candidate_data_dict['region_name'] = candidate.constituency.region.name if candidate.constituency.region else None
            election_candidate_data_dict['party_name'] = candidate.party.name
            election_candidate_data_dict['party_color_code'] = candidate.party.code
            election_candidate_data_dict['party_symbol_path'] = candidate.party.logo
            group_name = None
            group_id = None
            if candidate.party.group_set.all().exists():
                group = candidate.party.group_set.all()[0]
                group_id = group.id
                group_name = group.name
            election_candidate_data_dict['group_name'] = group_name
            election_candidate_data_dict['group_id'] = group_id
            election_candidate_data_dict['candidate_name'] = candidate.candidate.name
            election_candidate_data_dict['candidate_id'] = candidate.candidate.id
            election_candidate_data_dict['Education'] = candidate.candidate.education
            election_candidate_data_dict['Gender'] = candidate.candidate.gender
            election_candidate_data_dict['Age'] = candidate.candidate.age
            election_candidate_data_dict['Criminalcases'] = candidate.criminal_cases
            election_candidate_data_dict['Assets'] = candidate.assets
            election_candidate_data_dict['Total_Seats'] = Total_Seats
            votes_results = ResultVote.objects.filter(
                election_id=election,
                state_id=candidate.constituency.state.id,
                candidate_id=candidate.candidate.id,
            )
            won_status = None
            if votes_results.exists():
                votes_result = votes_results[0]
                if votes_result.is_result_final:
                    if votes_result.status=='L':
                        won_status='Won'
                    else:
                        won_status='Lost'
                won_status = votes_result.status
            election_candidate_data_dict['status'] = won_status
            old_election_candidates = old_election.electioncandidate_set.all()
            old_votes_results = ResultVote.objects.filter(
                election_id=old_election,
                state_id=candidate.constituency.state.id,
                constituency=candidate.constituency.id,
                status='L'
            )
            incumbent_candidate = None
            incumbent_party = None
            incumbent_party_color_code = None
            incumbent_party_symbol_path = None
            ind_party_color = None
            ind_party_symbol_path = None
            if old_votes_results.exists():
                old_votes_result = old_votes_results[0]
                incumbent_candidate = old_votes_result.candidate.name
                old_election_candidate_objs = old_election_candidates.filter(
                    candidate=old_votes_result.candidate, constituency=candidate.constituency
                )
                if old_election_candidate_objs.exists():
                    old_election_candidate_obj = old_election_candidate_objs[0]
                    old_election_candidate_obj.party.name
                    incumbent_party = old_election_candidate_obj.party.name
                    incumbent_party_color_code = old_election_candidate_obj.party.code
                    incumbent_party_symbol_path = old_election_candidate_obj.party.logo
            election_candidate_data_dict['phase_no'] = 1
            election_candidate_data_dict['incumbent_candidate'] = incumbent_candidate
            election_candidate_data_dict['incumbent_party'] = incumbent_party
            election_candidate_data_dict['incumbent_party_color_code'] = incumbent_party_color_code
            election_candidate_data_dict['incumbent_party_symbol_path'] = incumbent_party_symbol_path
            election_candidate_data_dict['ind_party_color'] = ind_party_color
            election_candidate_data_dict['ind_party_symbol_path'] = ind_party_symbol_path
            election_candidate_data_dict['acname'] = ''
            election_candidate_data_dict['assembly_id'] = ''
            election_candidate_data.append(election_candidate_data_dict)
        return Response(election_candidate_data, status=status.HTTP_200_OK)
    #         <assembly_id>1</assembly_id>
    # <acname>Abdasa</acname>
        """
            <state_id>102</state_id>
    <state_name>Gujarat</state_name>
    <region_id>66</region_id>
    <region_name>SAURASHTRA</region_name>

    <party_name>BJP</party_name>
    <group_name>BJP</group_name>
    <status>LOST</status>
    <candidate_name>Chhabilbhai Naranbhai Patel</candidate_name>
    <candidate_id>339</candidate_id>
    <party_color_code>#f78002</party_color_code>
    <party_symbol_path>http://timesofindia.indiatimes.com/photo/51774624.cms</party_symbol_path>
    <phase_no>1</phase_no>
    <Gender>M</Gender>
    <Age>56</Age>
    <Education>Graduate Professional</Education>
    <Criminalcases>0</Criminalcases>
    <Assets>Rs 4,49,26,080</Assets>
    <incumbent_candidate>CHHABILBHAI NARANBHAI PATEL</incumbent_candidate>
    <incumbent_party>CONG</incumbent_party>
    <incumbent_party_color_code>#127f32             </incumbent_party_color_code>
    <incumbent_party_symbol_path>http://timesofindia.indiatimes.com/photo/51774625.cms</incumbent_party_symbol_path>
    <candidate_unique_name />
    <ind_party_color>#f78002</ind_party_color>
    <ind_party_symbol_path>http://timesofindia.indiatimes.com/photo/51774624.cms</ind_party_symbol_path>
    <Total_Seats>182</Total_Seats>"""
        # parties = election_candidates.values_list('party', flat=True).distinct()
        # constituencies = election_candidates.values_list('constituency', flat=True).distinct()
        # states = election_candidates.values_list('constituency__state_id', flat=True).distinct()
        # ally_gain_lose_data_list = []
                # get party group what if there multiple groups for a party ? not possible?
                # what about ind_party_symbol_path? ind_party_color?

class CandidateVipFlashesViewset(viewsets.ModelViewSet):
    queryset = ResultVote.objects.all()
    serializer_class = ResultVoteSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        state_id = request.GET.get('state_id')
        election_id = request.GET.get('election_id')
        old_election_id = request.GET.get('old_election_id')
        if (not old_election_id) or (not election_id):
            election_id = 2
            old_election_id = 1
            # return Response(
            #     {'error': 'election_id and old_election_id is mandetory'},
            #     status=status.HTTP_400_BAD_REQUEST
            # )
        elections = Election.objects.filter(id=election_id)
        election = None
        if elections.exists():
            election = elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        old_election = None
        old_elections = Election.objects.filter(id=old_election_id)
        if old_elections.exists():
            old_election = old_elections[0]
        else:
            return Response(
                {'error': 'Please select the election'},
                status=status.HTTP_400_BAD_REQUEST
            )
        election_candidates = election.electioncandidate_set.filter(is_vip=True).all()
        election_candidate_data = []
        if state_id:
            election_candidates = election_candidates.filter(constituency__state_id=state_id)
        for candidate in election_candidates:
            Total_Seats = Constituency.objects.filter(
                state_id=candidate.constituency.state.id
            ).count()
            election_candidate_data_dict = dict()
            election_candidate_data_dict['acno'] = candidate.constituency_id
            election_candidate_data_dict['acname'] = candidate.constituency.name
            election_candidate_data_dict['state_id'] = candidate.constituency.state.id
            election_candidate_data_dict['state_name'] = candidate.constituency.state.name
            election_candidate_data_dict['party_name'] = candidate.party.name
            election_candidate_data_dict['party_color_code'] = candidate.party.code
            election_candidate_data_dict['party_symbol_path'] = candidate.party.logo
            # group_name = None
            # group_id = None
            # if candidate.party.group_set.all().exists():
            #     group = candidate.party.group_set.all()[0]
            #     group_id = group.id
            #     group_name = group.name
            # election_candidate_data_dict['group_name'] = group_name
            # election_candidate_data_dict['group_id'] = group_id
            election_candidate_data_dict['vip_name'] = candidate.candidate.name
            election_candidate_data_dict['candidate_id'] = candidate.candidate.id
            election_candidate_data_dict['Education'] = candidate.candidate.education
            election_candidate_data_dict['Gender'] = candidate.candidate.gender
            election_candidate_data_dict['Age'] = candidate.candidate.age
            election_candidate_data_dict['Criminalcases'] = candidate.criminal_cases
            election_candidate_data_dict['Assets'] = candidate.assets
            votes_results = ResultVote.objects.filter(
                election_id=election,
                state_id=candidate.constituency.state.id,
                candidate_id=candidate.candidate.id,
            )
            won_status = None
            votes = 0
            if votes_results.exists():
                votes_result = votes_results[0]
                votes = votes_result.votes
                if votes_result.is_result_final:
                    if votes_result.status=='L':
                        won_status='Won'
                    else:
                        won_status='Lost'
                won_status = votes_result.status
            election_candidate_data_dict['Votes'] = votes
            election_candidate_data_dict['status'] = won_status
            election_candidate_data.append(election_candidate_data_dict)
        return Response(election_candidate_data, status=status.HTTP_200_OK)
"""
  <Table>
    <state_id>102</state_id>
    <state_name>Gujarat</state_name>
    <acno>137</acno>
    <acname>Chhota Udaipur</acname>
    <vip_name>Mohansinh Chhotubhai Rathava</vip_name>
    <candidate_id>2080</candidate_id>
    <party_name>CONG</party_name>
    <status>WON</status>
    <Votes>0</Votes>
    <party_color_code>#127f32</party_color_code>
    <party_symbol_path>http://timesofindia.indiatimes.com/photo/51774625.cms</party_symbol_path>
    <phase_no>2</phase_no>
    <Gender>M</Gender>
    <Age>72</Age>
    <Education>10th Pass</Education>
    <Criminalcases>0</Criminalcases>
    <Assets>Rs 1,50,39,979</Assets>
    <image_path />
    <candidate_unique_name />
  </Table>
"""
class ResultVoteViewSet(viewsets.ModelViewSet):
    queryset = ResultVote.objects.all()
    serializer_class = ResultVoteSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self, *args, **kwargs):
        serializer = ResultVoteSerializer
        if self.request.method in ['PATCH', 'POST', 'PUT']:
            serializer = ResultVoteHistoryPostSerializer
        return serializer

    def create(self, request, *args, **kwargs):
        user = request.user.userprofile
        batch_uuid = uuid.uuid4()
        for result in request.data:
            result.update({'uuid': batch_uuid.get_hex(), 'user': user.id})
            result_vote, created = ResultVote.objects.get_or_create(
                user_id=result['user'],
                election_id=result['election'],
                candidate_id=result['candidate'],
                constituency_id=result['constituency'],
                state_id=result['state']
            )
            # only one user should send data for one counting center
            result_vote.level = result['level']
            result_vote.status = result.get('status')
            result_vote.uuid = result['uuid']
            result_vote.votes = result['votes']
            result_vote.level = result['level']
            result_vote.save()
            
        serializer = self.get_serializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ElectionDataViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = ElectionDataSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user.userprofile)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PartyViewSet(viewsets.ModelViewSet):
    queryset = Party.objects.all()
    serializer_class = PartySerializer
    permission_classes = (AllowAny,)


class StateViewSet(viewsets.ModelViewSet):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    permission_classes = (AllowAny,)


class ConstituencyViewSet(viewsets.ModelViewSet):
    queryset = Constituency.objects.all()
    serializer_class = ConstituencySerializer
    permission_classes = (AllowAny,)


class CandidateViewSet(viewsets.ModelViewSet):
    queryset = Candidate.objects.all()
    serializer_class = CandidateSerializer


class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializerNormal
    permission_classes = (IsAuthenticated,)
    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user.userprofile)
        return Response(serializer.data, status=status.HTTP_200_OK)
