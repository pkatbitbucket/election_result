# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import F, Sum, FloatField, Avg, Max, Q
from rest_framework import serializers
from winner.models import Party, State, Constituency, Candidate, Region
from winner.models import UserConstituency, UserProfile, ResultVote
from winner.models import ElectionCandidate, Group, Election, ResultVoteHistory


class PartySerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Party
        fields = '__all__'


class GroupSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Group
        fields = '__all__'


class CandidateSerializer(serializers.ModelSerializer):
    party = PartySerializer()

    class Meta(object):
        model = Candidate
        fields = '__all__'


class ConstituencySerializer(serializers.ModelSerializer):
    candidates = serializers.SerializerMethodField()

    def get_candidates(self, obj):
        return CandidateSerializer(obj.candidate_set.all(), many=True)

    class Meta(object):
        model = Constituency
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):
    constuencies = serializers.SerializerMethodField()

    def get_constuencies(self, obj):
        return ConstituencySerializer(obj.constituency_set.all(), many=True).data

    class Meta(object):
        model = State
        fields = '__all__'


class RegionSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Region
        fields = '__all__'


class UserProfileSerializerNormal(serializers.ModelSerializer):
    class Meta(object):
        model = UserProfile
        fields = '__all__'


class UserProfileSerializer(serializers.ModelSerializer):
    states = serializers.SerializerMethodField()

    def get_states(self, obj):
        constuencies = obj.userconstituency_set.values_list('constituency', flat=True).all()
        states = Constituency.objects.filter(id__in=constuencies).values_list('state', flat=True).all()
        serializer = StateSerializer(State.objects.filter(id__in=states).all(), many=True)
        return serializer.data

    class Meta(object):
        model = UserProfile
        fields = '__all__'


class UserConstituencySerializer(serializers.ModelSerializer):
    class Meta(object):
        model = UserConstituency
        fields = '__all__'


class ResultVoteListSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        resultvotes = [ResultVote(**item) for item in validated_data]
        return ResultVote.objects.bulk_create(resultvotes)


class ResultVoteHistoryPostSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = ResultVoteHistory
        # list_serializer_class = ResultVoteListSerializer
        fields = '__all__'


class ResultVoteSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = ResultVote
        fields = '__all__'


class ConstituencyUserSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Constituency
        fields = '__all__'


class ElectionUserSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Election
        fields = '__all__'


class ElectionCandidateSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = ElectionCandidate
        fields = '__all__'


class CandidateUserSerializer(serializers.ModelSerializer):
    election_candidate = serializers.SerializerMethodField()

    def get_election_candidate(self, obj):
        return ElectionCandidateSerializer(obj.electioncandidate_set.all()[0]).data

    class Meta(object):
        model = Candidate
        fields = '__all__'


class StateUserSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = State
        fields = '__all__'


class ElectionDataSerializer(serializers.ModelSerializer):
    constituencies = serializers.SerializerMethodField()
    candidates = serializers.SerializerMethodField()
    states = serializers.SerializerMethodField()
    elections = serializers.SerializerMethodField()
    groups = serializers.SerializerMethodField()
    parties = serializers.SerializerMethodField()

    def get_constituencies(self, obj):
        all_constituency = UserConstituency.objects.filter(user=obj).values_list(
            'constituency', flat=True
        ).all()
        all_constituency = Constituency.objects.filter(id__in=all_constituency).all()
        return ConstituencyUserSerializer(all_constituency, many=True).data

    def get_states(self, obj):
        all_constituency = UserConstituency.objects.filter(user=obj).values_list(
            'constituency', flat=True
        ).all()
        states = Constituency.objects.filter(
            id__in=all_constituency
        ).values_list('state', flat=True)
        states = State.objects.filter(id__in=states)
        return StateUserSerializer(states, many=True).data

    def get_elections(self, obj):
        all_election = UserConstituency.objects.filter(user=obj).values_list(
            'election', flat=True
        ).all()
        all_election = Election.objects.filter(id__in=all_election)
        return ElectionUserSerializer(all_election, many=True).data

    def get_candidates(self, obj):
        # import  ipdb; ipdb.set_trace()
        candidates_ids = []
        for user_conti in UserConstituency.objects.filter(user=obj):
            candidates = ElectionCandidate.objects.filter(
                election=user_conti.election,
                constituency=user_conti.constituency
            ).values_list('candidate', flat=True).all()
            candidates_ids.extend(list(candidates))
        all_candidates = Candidate.objects.filter(id__in=candidates_ids).all()
        return CandidateUserSerializer(all_candidates, many=True).data

    def get_parties(self, obj):
        parties_ids = []
        for user_conti in UserConstituency.objects.filter(user=obj):
            parties = ElectionCandidate.objects.filter(
                election=user_conti.election,
                constituency=user_conti.constituency
            ).values_list('party', flat=True).all()
            parties_ids.extend(list(parties))
        all_parties = Party.objects.filter(id__in=parties_ids).all()
        return PartySerializer(all_parties, many=True).data

    def get_groups(self, obj):
        groups_ids = []
        for user_conti in UserConstituency.objects.filter(user=obj):
            groups = ElectionCandidate.objects.filter(
                election=user_conti.election,
                constituency=user_conti.constituency
            ).values_list('group', flat=True).all()
            groups_ids.extend(list(groups))
        groups_ids = Group.objects.filter(id__in=groups_ids).all()
        return GroupSerializer(groups_ids, many=True).data

    class Meta(object):
        model = UserProfile
        fields = '__all__'
