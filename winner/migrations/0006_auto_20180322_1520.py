# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-03-22 15:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('winner', '0005_auto_20180322_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resultvotehistory',
            name='votes',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
