# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


class Election(models.Model):
    ASSEMBLY = 'A'
    PARLIAMENT = 'P'
    TYPE_OPTIONS = (
        (ASSEMBLY, 'Assembly'),
        (PARLIAMENT, 'Parliament'),
    )
    NEW = 'N'
    COMPLETED = 'C'
    STATUS_OPTIONS = (
        (NEW, 'New'),
        (COMPLETED, 'Completed'),
    )
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=32, null=True, blank=True)
    type = models.CharField(max_length=3, choices=TYPE_OPTIONS, null=True, blank=True)
    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Party(models.Model):
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=32, null=True, blank=True)
    abbr = models.CharField(max_length=16, null=True, blank=True)
    logo = models.CharField(max_length=256, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=64)
    code = models.CharField(max_length=16, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Region(models.Model):
    name = models.CharField(max_length=64)
    code = models.CharField(max_length=16)
    state = models.ForeignKey(State, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Constituency(models.Model):
    name = models.CharField(max_length=64)
    state = models.ForeignKey(State)
    region = models.ForeignKey(Region, null=True, blank=True)
    code = models.CharField(max_length=32, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Candidate(models.Model):
    MALE = 'M'
    FEMALE = 'F'
    FEMALE = 'O'
    GENDER_OPTIONS = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
        (FEMALE, 'Other'),
    )
    name = models.CharField(max_length=128)
    gender = models.CharField(max_length=8, choices=GENDER_OPTIONS, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    education = models.CharField(max_length=128, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Group(models.Model):
    election = models.ForeignKey(Election, null=True, blank=True)
    name = models.CharField(max_length=64)
    candidates = models.ManyToManyField(Candidate, null=True, blank=True)
    parties = models.ManyToManyField(Party, null=True, blank=True)

    def __unicode__(self):
        return self.name


class ElectionCandidate(models.Model):
    election = models.ForeignKey(Election)
    candidate = models.ForeignKey(Candidate)
    code = models.CharField(max_length=64, null=True, blank=True)
    constituency = models.ForeignKey(Constituency)
    party = models.ForeignKey(Party)
    group = models.ForeignKey(Group, null=True, blank=True)
    is_vip = models.BooleanField(default=False)
    phase = models.IntegerField(null=True, blank=True)
    criminal_cases = models.IntegerField(null=True, blank=True)
    assets = models.CharField(max_length=8, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.candidate.name


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length=64)
    mobile = models.CharField(max_length=16, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class UserConstituency(models.Model):
    user = models.ForeignKey(UserProfile)
    election = models.ForeignKey(Election)
    constituency = models.ForeignKey(Constituency)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{0}-{1}".format(self.user.name, self.constituency.name)


class ElectionResult(models.Model):
    election = models.ForeignKey(Election)
    election_candidate = models.ForeignKey(ElectionCandidate)
    candidate = models.ForeignKey(Candidate)
    state = models.ForeignKey(State)
    region = models.ForeignKey(Region, null=True, blank=True)
    constituency = models.ForeignKey(Constituency)
    party = models.ForeignKey(Party)
    group = models.ForeignKey(Group, null=True, blank=True)
    status = models.CharField(max_length=32, null=True, blank=True)
    vote = models.IntegerField(null=True, blank=True)
    percent = models.FloatField(null=True, blank=True)
    total_seats = models.IntegerField(null=True, blank=True)
    margin = models.IntegerField(default=True)
    date = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{0}-{1}".format(
            self.election_candidate.candidate.name, self.constituency.name
        )


class ResultVote(models.Model):
    user = models.ForeignKey(UserProfile)
    election = models.ForeignKey(Election)
    candidate = models.ForeignKey(Candidate)
    constituency = models.ForeignKey(Constituency)
    state = models.ForeignKey(State)
    level = models.IntegerField(default=0, null=True, blank=True)
    votes = models.IntegerField(null=True, blank=True)
    status = models.CharField(max_length=24, null=True, blank=True)
    is_result_final = models.BooleanField(default=False)
    uuid = models.CharField(max_length=64, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{0}-{1}-{2}".format(
            self.candidate.name, self.constituency.name, self.state.name
        )


class ResultVoteHistory(models.Model):
    user = models.ForeignKey(UserProfile)
    election = models.ForeignKey(Election)
    candidate = models.ForeignKey(Candidate)
    constituency = models.ForeignKey(Constituency)
    state = models.ForeignKey(State)
    level = models.IntegerField(default=0)
    status = models.CharField(max_length=24, null=True, blank=True)
    votes = models.IntegerField(null=True, blank=True)
    uuid = models.CharField(max_length=64, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
